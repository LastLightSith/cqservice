package xyz.smit17.CQUserReader;

import java.io.Serializable;

// either by id or uname
public class UserIdentifier implements Serializable {
    private String uname;
    private Long id; // Long can be null long can't

    public Long getId() {
        return id;
    }

    public String getUname() {
        return uname;
    }

    public UserIdentifier(String uname) {
        this.uname = uname;
    }

    public UserIdentifier(Long id) {
        this.uname = null;
    }
}