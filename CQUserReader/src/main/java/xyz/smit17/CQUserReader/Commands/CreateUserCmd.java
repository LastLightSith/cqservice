package xyz.smit17.CQUserReader.Commands;

import java.util.Date;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;

public class CreateUserCmd {
    @NotNull
    private String uname;
    @NotNull
    private String passwd;
    @NotNull
    private String email;
    @NotNull
    @JsonFormat(pattern="yyyy-MM-dd")
    private Date dob;

    public String getUname() {
        return uname;
    }

    public String getPasswd() {
        return passwd;
    }

    public String getEmail() {
        return email;
    }

    public Date getDob() {
        return dob;
    }
}