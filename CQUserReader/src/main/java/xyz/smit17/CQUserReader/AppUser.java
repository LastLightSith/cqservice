package xyz.smit17.CQUserReader;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
public class AppUser {
    @Id
    @GeneratedValue
    Long id;

    @NotNull
    @Column(unique = true)
    public String uname;

    @NotNull
    public String passwd;

    @NotNull
    public String email;

    @NotNull
    public Date dob;

    public AppUser(){}
    public AppUser(Long id, @NotNull String uname, @NotNull String passwd, @NotNull String email, @NotNull Date dob) {
        this.id = id;
        this.uname = uname;
        this.passwd = passwd;
        this.email = email;
        this.dob = dob;
    }
}