package xyz.smit17.CQUserReader;

import java.util.Vector;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ReadController {

    @Autowired
    private UserRepo userRepo;

    @GetMapping("/user")
    public Vector<AppUser> gerUserById() {
        Vector<AppUser> list = new Vector<>();
        userRepo.findAll().iterator().forEachRemaining(list::add);
        return list;
    }

    @GetMapping("/user/id/{uid}")
    public AppUser gerUserById(@PathVariable long uid) {
        return userRepo.findById(uid).get();
    }

    @GetMapping("/user/uname/{uname}")
    public AppUser gerUserById(@PathVariable String uname) {
        return userRepo.findByUname(uname).get();
    }
}