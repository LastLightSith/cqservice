package xyz.smit17.CQUserReader;

import java.util.Optional;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.repository.CrudRepository;

interface UserRepo extends CrudRepository<AppUser, Long> {
    Long deleteByUname(String uname);

    Optional<AppUser> findByUname(String uname);
}

class CommandWrapper {
    public String type;
    public String cmd;

    Object innerCmd(Class<?> c) {
        var mapper = new ObjectMapper();
        Object cmp2 = null;
        try {
            cmp2 = mapper.readValue(cmd, c);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return cmp2;
    }
}

@SpringBootApplication
public class CqUserReaderApplication {

    public static void main(String[] args) {
        SpringApplication.run(CqUserReaderApplication.class, args);
    }

}
