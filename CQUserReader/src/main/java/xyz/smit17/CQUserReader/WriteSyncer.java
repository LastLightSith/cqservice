package xyz.smit17.CQUserReader;

import java.util.function.Consumer;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import xyz.smit17.CQUserReader.Commands.CreateUserCmd;
import xyz.smit17.CQUserReader.Commands.RemoveUserCmd;
import xyz.smit17.CQUserReader.Commands.UpdateUserCmd;

@Service
class WriteSyncer {

    @Autowired
    private UserRepo userRepo;

    @Bean
    public Consumer<String> handleCommand() {
        return cmdWrapper -> {

            ObjectMapper mapper = new ObjectMapper();
            CommandWrapper cmd;
            try {
                cmd = mapper.readValue(cmdWrapper, CommandWrapper.class);
            } catch (Exception e) {
                e.printStackTrace();
                return;
            }

            System.err.println("got cmd.type: " + cmd.type + " cmd.cmd " + cmd.cmd);

            switch (cmd.type) {
                case "createUser": {
                    CreateUserCmd rcmd = (CreateUserCmd) cmd.innerCmd(CreateUserCmd.class);
                    System.err.println("got cmd.type: " + rcmd.getUname() + " cmd.cmd " + rcmd.getEmail());
                    userRepo.save(new AppUser(null, rcmd.getUname(), rcmd.getPasswd(), rcmd.getEmail(), rcmd.getDob()));
                    break;
                }
                case "removeUser": {
                    RemoveUserCmd rcmd = (RemoveUserCmd) cmd.innerCmd(RemoveUserCmd.class);

                    var id = rcmd.uid.getId();
                    var uname = rcmd.uid.getUname();

                    if (id != null)
                        userRepo.deleteById(id);
                    else
                        userRepo.deleteByUname(uname);

                    break;
                }
                case "updateUser": {
                    UpdateUserCmd rcmd = (UpdateUserCmd) cmd.innerCmd(UpdateUserCmd.class);

                    var id = rcmd.uid.getId();
                    var uname = rcmd.uid.getUname();

                    AppUser user = null;
                    if (id != null) {
                        user = userRepo.findById(id).get();
                    } else {
                        user = userRepo.findByUname(uname).get();
                    }

                    if (rcmd.getUname() != null)
                        user.uname = rcmd.getUname();

                    if (rcmd.getDob() != null)
                        user.dob = rcmd.getDob();

                    if (rcmd.getEmail() != null)
                        user.dob = rcmd.getDob();

                    if (rcmd.getPasswd() != null)
                        user.passwd = rcmd.getPasswd();

                    userRepo.save(user);
                    break;
                }
            }
            ;
        };
    }

}