This is my first spring boot project. It tries to implement CQRS design pattern

## How to run

1. Start rabbitmq and postgresql server

on ArchLinux:-

```bash
systemctl start rabbitmq.service
systemctl status postgresql.service
```
2. Then first start CQUSerWriter and then CQUserReader in respective directories
```bash
mvn spring-boot:run
```
Writer will start on port 8080 and Reader will start on 8090

## REST APIs

following commands are exposed via rest api

CreateUser, RemoveUser, UpdateUser

example:-
```bash
curl -X POST http://0.0.0.0:8080/createUser -H 
'Content-Type: application/json' -d '{
    "uname" : "koolCat12",
    "dob" : "0001-02-0010",
    "email" : "kat@lion.kity",
    "passwd" : "maomao"
}'
```

This will return 200 and will create new createUser command which on success will create UserCreated event. These events are stored in writer database and then relevant of them are passed to reader via spring cloud stream on top of rabbitmq and then are applied to reader database

Similar commands are also available for update and deletion

To read the newly created user, use follwing requst:-

```bash
curl -X GET http://0.0.0.0:8090/user/uname/koolCat12
```