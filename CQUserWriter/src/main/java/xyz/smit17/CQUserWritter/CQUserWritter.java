package xyz.smit17.CQUserWritter;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CQUserWritter {

    public static void main(String[] args) {
        SpringApplication.run(CQUserWritter.class, args);
    }

}

