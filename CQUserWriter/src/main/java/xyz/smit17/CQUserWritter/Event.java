package xyz.smit17.CQUserWritter;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.SerializationUtils;

enum EventType {
    UserCreated,
    UserRemoved,
    UserUpdated,
    // UserEmailValidated,
}

interface _ser extends Serializable {
    public default byte[] ser() {
        return SerializationUtils.serialize(this);
    }
}

class UserCreatedEvent implements Serializable {
    private String uname;
    private String passwd;
    private String email;
    private Date dob;

    public UserCreatedEvent(String uname, String passwd, String email, Date dob) {
        this.uname = uname;
        this.passwd = passwd;
        this.email = email;
        this.dob = dob;
    }

    public String getUname() {
        return uname;
    }

    public String getPasswd() {
        return passwd;
    }

    public String getEmail() {
        return email;
    }

    public Date getDob() {
        return dob;
    }
}

class UserUpdatedEvent implements Serializable {
    @NotNull
    private UserIdentifier uid;

    private String uname;
    private String passwd;
    private String email;
    private Date dob;

    public UserUpdatedEvent(@NotNull UserIdentifier uid, String uname, String passwd, String email, Date dob) {
        this.uid = uid;
        this.uname = uname;
        this.passwd = passwd;
        this.email = email;
        this.dob = dob;
    }

    public UserIdentifier getUid() {
        return uid;
    }

    public String getUname() {
        return uname;
    }

    public String getPasswd() {
        return passwd;
    }

    public String getEmail() {
        return email;
    }

    public Date getDob() {
        return dob;
    }
}

class UserRemovedEvent implements Serializable {
    private UserIdentifier uid;

    public UserRemovedEvent(UserIdentifier uid) {
        this.uid = uid;
    }

    public UserIdentifier getUid() {
        return uid;
    }
}

@Entity
class Event {

    @Id
    @GeneratedValue
    long id;

    @NotNull
    Date at;

    @NotNull
    EventType type;

    byte[] payload;

    public Date getAt() {
        return at;
    }

    public byte[] getPayload() {
        return payload;
    }

    public Event(EventType type, byte[] payload) {
        at = new Date();
        this.type = type;
        this.payload = payload;
    }

    //helper methodsQ

    public static Event userCreatedEvent(String uname, String passwd, String email, Date dob) {
        return new Event(
            EventType.UserCreated,
            SerializationUtils.serialize(new UserCreatedEvent(uname, passwd, email, dob))
        );
    }

    public static Event userUpdatedEvent(UserIdentifier uid, String uname, String passwd, String email, Date dob) {
        return new Event(
            EventType.UserUpdated,
            SerializationUtils.serialize(new UserUpdatedEvent(uid, uname, passwd, email, dob))
        );
    }

    public static Event userRemovedEvent(UserIdentifier uid) {
        return new Event(
            EventType.UserCreated,
            SerializationUtils.serialize(new UserRemovedEvent(uid))
        );
    }
}