package xyz.smit17.CQUserWritter.Commands;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import xyz.smit17.CQUserWritter.UserIdentifier;

public class UpdateUserCmd {
    @NotNull
    private UserIdentifier uid;
    private String uname;
    private String passwd;
    private String email;
    private Date dob;

    @JsonCreator(mode = JsonCreator.Mode.PROPERTIES)
    public UpdateUserCmd(
            @NotNull @JsonProperty UserIdentifier uid,
            @JsonProperty String uname,
            @JsonProperty String passwd,
            @JsonProperty String email,
            @JsonProperty Date dob) {

        if (uname == null && passwd == null && null == dob) {
            // todo: do proper error handling
            System.err.println("invalid command");
        }

        this.uid = uid;
        this.uname = uname;
        this.passwd = passwd;
        this.email = email;
        this.dob = dob;
    }

    public UserIdentifier getUid() {
        return uid;
    }

    public String getUname() {
        return uname;
    }

    public String getPasswd() {
        return passwd;
    }

    public String getEmail() {
        return email;
    }

    public Date getDob() {
        return dob;
    }
}