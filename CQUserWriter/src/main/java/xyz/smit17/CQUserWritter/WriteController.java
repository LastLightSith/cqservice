package xyz.smit17.CQUserWritter;

import java.io.Serializable;
import java.util.function.Consumer;
import java.util.function.Supplier;

import javax.validation.Valid;
import javax.websocket.server.PathParam;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.commons.lang3.SerializationUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.function.StreamBridge;
import org.springframework.context.annotation.Bean;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import xyz.smit17.CQUserWritter.Commands.CreateUserCmd;
import xyz.smit17.CQUserWritter.Commands.RemoveUserCmd;
import xyz.smit17.CQUserWritter.Commands.UpdateUserCmd;

@Service
interface EventStoreRepo extends CrudRepository<Event, Long> {
}

class CommandWrapper<T> {
    public String type;
    public String cmd;

    public CommandWrapper(String type, T cmd) {
        this.type = type;
        ObjectMapper mapper = new ObjectMapper();
        try {
            this.cmd = mapper.writeValueAsString(cmd);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    public String json() {
        ObjectMapper mapper = new ObjectMapper();
        try {
            String j = mapper.writeValueAsString(this);
            System.err.println("sending cmd: "+j);
            return j;
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            System.exit(-1);
            return null;
        }
    }
}

@RestController
class WriteController {

    @Autowired
    private EventStoreRepo evnRepo;

    @Autowired
    private StreamBridge streamBridge;

    // @GetMapping("/send/{val}")
    // public void testfn(@PathVariable String val) {
    //     var k = new Lol();
    //     k.f1 = val;
    //     k.f2 = "ehhe "+ val;
    //     streamBridge.send("testfnOut", k);
    // }

    @PostMapping("/createUser")
    public void createUser(@RequestBody @Valid CreateUserCmd cmd) {

        System.err.println(cmd.getUname());

        //todo:- check if username available

        //todo:- password to hash
        

        evnRepo.save(Event.userCreatedEvent(
                cmd.getUname(), cmd.getPasswd(), cmd.getEmail(), cmd.getDob()));
        
        streamBridge.send("syncState", new CommandWrapper<>("createUser",cmd).json());

    }

    @PostMapping("/removeUser")
    public void removeUser(@RequestBody RemoveUserCmd cmd) {
        evnRepo.save(Event.userRemovedEvent(cmd.uid));

        streamBridge.send("syncState", new CommandWrapper<>("removedUser",cmd).json());
    }

    @PostMapping("/updateUser")
    public void updateUser(@RequestBody UpdateUserCmd cmd) {
        evnRepo.save(
                Event.userUpdatedEvent(cmd.getUid(), cmd.getUname(), cmd.getPasswd(), cmd.getEmail(), cmd.getDob()));

        streamBridge.send("syncState", new CommandWrapper<>("updateUser",cmd).json());
    }
}
